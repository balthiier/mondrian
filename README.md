Mondrian App
===================


A web application that have the goal to calculate the clients costs with material and hand work and check with details each parte of each table project.

----------
###Installation for use

    npm run start || node server.js

###Main Dependencies
- Mongoose
- React
- Redux
- Express
- Body-parser

Lisence
===================

MIT. Software open source, free for be distributed and for public usage.

----------