import axios from 'axios'
import URL from '../config/config'

// create table in DB
export const createTable = () => {
    return axios.post(`${URL.host}createtable/`)
}

// delete table by ID in DB
export const deleteTable = id => {
    return axios.delete(`${URL.host}deletetable/`, {data: {_id: id}})
}

// get tables from DB
export const showTables = () => {
    return axios.get(`${URL.host}showtables/`)
}

// update selected Table in DB
export const updateTable = ({_id, products, uniqueID}) => {
    return axios.patch(`${URL.host}updatetable/`, {
        data: {
            _id,
            initialState: {
                products: products,
                uniqueID: uniqueID
            }
        }
    })
}