import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
Enzyme.configure({ adapter: new Adapter() })

import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

import ProductForm from '../components/ProductForm'

test('Form submit must have correctly types', () => {
    const defaultState = { productName: '', withTax: 0, noTax: 0, tax: 25, quantity: 1 }

    const productForm = Enzyme.shallow(<ProductForm/>)

    expect(productForm.state()).toEqual(defaultState)
    expect(productForm.containsAllMatchingElements([<TextField/>, <RaisedButton label="Adicionar" primary={true} type="submit"/>])).toEqual(true)
})