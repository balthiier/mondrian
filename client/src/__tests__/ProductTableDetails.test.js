import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
Enzyme.configure({ adapter: new Adapter() })

import {TableRow, TableRowColumn} from 'material-ui/Table'

test('ProductTableDetails should render correctly', () => {
    const products = [
        {
            productName: 'parafuso',
            withTax: '5.59',
            noTax: '22.35',
            tax: 25,
            quantity: 23,
            uniqueID: 0
        },
        {
            productName: 'madeiras',
            withTax: '5.55',
            noTax: '22.22',
            tax: 25,
            quantity: 25,
            uniqueID: 1
        }
    ]

    const productTable = Enzyme.shallow(
        <div>
            {products.map((product, i) => {
                return (
                    <TableRow key={product.uniqueID}>
                        <TableRowColumn style={{width: 30}}>{product.uniqueID}</TableRowColumn>
                        <TableRowColumn>{product.productName}</TableRowColumn>
                        <TableRowColumn>R$ {product.noTax.replace('.', ',')}</TableRowColumn>
                        <TableRowColumn>R$ {product.withTax.replace('.', ',')}</TableRowColumn>
                        <TableRowColumn>{product.quantity}</TableRowColumn>
                        <TableRowColumn>{product.tax}%</TableRowColumn>
                        <TableRowColumn>R$ {(product.quantity * product.noTax).toFixed(2).toString().replace('.', ',')}</TableRowColumn>
                    </TableRow>
                )
            })}
        </div>
    )

    expect(productTable.children().length).toEqual(2)
    expect(productTable.containsAllMatchingElements([TableRow, TableRowColumn])).toEqual(true)
    expect(productTable).toMatchSnapshot()
})

