import * as ProductActionType from '../actionTypes/product'

export const addProduct = product => {
	return {
		type: ProductActionType.ADD_PRODUCT,
		product
	}
}

export const selectProduct = selectedProducts => {
	return {
		type: ProductActionType.SELECT_PRODUCT,
		index: selectedProducts
	}
}

export const removeProduct = () => {
	return {
		type: ProductActionType.REMOVEALL_PRODUCT
	}
}