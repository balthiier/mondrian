import * as SnackbarActionTypes from '../actionTypes/snackbar'

export const toggleSnackbar = () => {
	return {
        type: SnackbarActionTypes.TOGGLE_SNACKBAR
	}
}