import * as TableActionType from '../actionTypes/table'

export const populateTable = tables => {
	return {
        type: TableActionType.POPULATE_TABLE,
		tables
	}
}

export const selectTable = table => {
	return {
		type: TableActionType.SELECT_TABLE,
		table
	}
}

export const changeName = name => {
	return {
		type: TableActionType.CHANGE_NAME_TABLE,
		name
	}
}