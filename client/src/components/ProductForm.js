import React, { Component } from 'react'

import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

export default class ProductForm extends Component {
	state = {
		productName: '',
		withTax: 0,
		noTax: 0,
		tax: 25,
		quantity: 1
	}
	
	updateTax = (val) => {
	    return this.setState({tax: val})
	}

	submitFormProduct = e => {
	    e.preventDefault()

	    const objectProduct = {
	        productName: this.state.productName,
	        withTax: (this.state.noTax * this.state.tax / 100).toFixed(2).toString(),
	        noTax: this.state.noTax.toFixed(2).toString(),
			tax: this.state.tax,
			quantity: this.state.quantity
	    }

	    // call add product action to reducer
	    this.props.addProduct(objectProduct)

		// reset fields
	    return this.setState({
	        productName: '',
			noTax: 0,
			quantity: 1
	    })
	}

	render() {
		return (
			<form style={style.containerApp} onSubmit={this.submitFormProduct}>
				<TextField 
					type="text" 
					floatingLabelText="Nome do produto" 
					minLength={3} 
					maxLength={100} 
					value={this.state.productName} 
					onChange={e => this.setState({productName: e.target.value})} 
					required
				/>

				<TextField 
					type="text" 
					min={1}
					maxLength={4}
					floatingLabelText="Quantidade" 
					style={{margin: '0 1em', width: 50}} 
					value={this.state.quantity}
					onChange={e => {
						let quantityValue = e.target.value

						if(/^[0-9]+$/.test(quantityValue)) return this.setState({quantity: quantityValue * 100 / 100})
					}}
				/>

				<TextField 
					type="number" 
					min={0} 
					floatingLabelText="Valor sem desconto" 
					style={style.input} 
					value={this.state.noTax}
					step={0.01} 
					onChange={e => this.setState({noTax: e.target.value * 100 / 100})}
				/>

				<TextField 
					type="text" 
					min={0} 
					disabled={true} 
					value={'R$' + (this.state.noTax * this.state.tax / 100).toFixed(2)} 
					floatingLabelText="Valor com desconto" 
					style={style.input}
				/>

				<TextField 
					type="number" 
					max={100} 
					min={0} 
					value={this.state.tax} 
					floatingLabelText="Desconto" 
					style={style.tax}
					step={0.01}
					onChange={e => {
						const taxValue = e.target.value
						if(taxValue > 100) return this.updateTax(100)
						if(taxValue < 0) return this.updateTax(0)
						return this.updateTax(taxValue)
					}}
				/>

				<RaisedButton label="Adicionar" primary={true} type="submit"/>
			</form>
		)
    }
}

const style = {
	containerApp: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		maxWidth: 1200,
		margin: '0 auto',
		backgroundColor: '#fafafa',
		borderRadius: 5,
		boxShadow: '0 2px 5px rgba(0,0,0,0.3)'
	},
  
	input: {
		margin: '0 .5em'
	},
  
	tax: {
		maxWidth: 50,
		marginRight: 25
	}
}
// CODE CLEANED!