import React from 'react'
import { connect } from 'react-redux'

import {Table, TableBody, TableHeader, TableFooter, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'

const ProductTableDetails = ({products, selectedProducts}) => {
	const discount = products.reduce((actual, product) => (actual * 1 + product.withTax * product.quantity * 100 / 100), 0)
	const noDiscount = products.reduce((actual, product) => (actual * 1 + product.noTax * product.quantity * 100 / 100), 0)

	return (
		<div className="table" style={style.tableList}>
			<Table height="300px" multiSelectable={true} fixedHeader={true} onRowSelection={rows => selectedProducts(rows)}>
				<TableHeader enableSelectAll={false}>
					<TableRow>
						<TableHeaderColumn style={{width: 30}}>Item</TableHeaderColumn>
						<TableHeaderColumn>Nome do produto</TableHeaderColumn>
						<TableHeaderColumn tooltip="Produto com valor bruto" style={{width: 111}}>Produto s/ desconto</TableHeaderColumn>
						<TableHeaderColumn tooltip="Produto com valor líquido" style={{width: 111}}>Produto c/ desconto</TableHeaderColumn>
						<TableHeaderColumn tooltip="Quantidades do produto" style={{width:40}}>QTD</TableHeaderColumn>
						<TableHeaderColumn tooltip="Preço Material em R$" style={{width: 80}}>Material</TableHeaderColumn>
						<TableHeaderColumn tooltip="Preço Mão de Obra em R$" style={{width: 80}}>Mão de obra</TableHeaderColumn>
						<TableHeaderColumn tooltip="BDI em R$ e %" style={{width: 100}}>BDI</TableHeaderColumn>
						<TableHeaderColumn tooltip="Total sem desconto" style={{width: 111}}>Total</TableHeaderColumn>
					</TableRow>
				</TableHeader>
				<TableBody deselectOnClickaway={false}>
					{products.map((product, i) => {
						return (
							<TableRow key={product.uniqueID}>
								<TableRowColumn style={{width: 30}}>{product.uniqueID}</TableRowColumn>
								<TableRowColumn>{product.productName}</TableRowColumn>
								<TableRowColumn style={{width: 111}}>R$ {product.noTax.replace('.', ',')}</TableRowColumn>
								<TableRowColumn style={{width: 111}}>R$ {((product.noTax * 100 / 100) - (product.withTax * 100 / 100)).toFixed(2).toString().replace('.', ',')}</TableRowColumn>
								<TableRowColumn style={{width:40}}>{product.quantity}</TableRowColumn>
								<TableRowColumn style={{width: 80}}>R$ {(product.noTax * 0.60).toFixed(2).toString().replace('.', ',')}</TableRowColumn>
								<TableRowColumn style={{width: 80}}>R$ {(product.noTax * 0.40).toFixed(2).toString().replace('.', ',')}</TableRowColumn>
								<TableRowColumn style={{width: 100}}>R$ {product.withTax.replace('.', ',')}|{product.tax}%</TableRowColumn>
								<TableRowColumn style={{width: 111}}>R$ {(product.quantity * product.noTax).toFixed(2).toString().replace('.', ',')}</TableRowColumn>
							</TableRow>
						)
					})}
				</TableBody>
				<TableFooter>
					<TableRow>
						<TableRowColumn 
							className="totalCell" 
							style={{textAlign: 'right', paddingBottom: 25, fontSize: 25, fontWeight: '400'}}
						>
							TOTAL: 
							<span> </span>
							S/BDI: R$ {(noDiscount - discount).toFixed(2).toString().replace('.', ',')} 
							<span> | </span>
							C/BDI: R$ {noDiscount.toFixed(2).toString().replace('.', ',')}
						</TableRowColumn>
					</TableRow>
				</TableFooter>
			</Table>
		</div>
	)
}

const style = {   
	tableList: {
		marginTop: '1em',
		position: 'absolute',
		bottom: 0
	}
}
  
const mapStateToProps = ({initialState}) => {
	return {
		products: [...initialState.products]
	}
}

export default connect(mapStateToProps)(ProductTableDetails)
// CODE CLEANED!