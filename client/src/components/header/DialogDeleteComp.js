import React, { Component } from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as ProductActionCreators from '../../actions/product'
import * as TableActionCreators from '../../actions/table'
import {updateTable} from '../../API/ProductAPI'

import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'

class DialogDeleteComp extends Component {
	state = {
		confirmModal: false
	}

	deleteData = () => {
		const {toggleModal, _id, dispatch} = this.props
		const cleanTableAction = bindActionCreators(ProductActionCreators.removeProduct, dispatch)
		const updateTableAction = bindActionCreators(TableActionCreators.populateTable, dispatch)

		// clean table then update all tables
		updateTable({_id}, [], 0)
		.then(({data}) => [cleanTableAction(), updateTableAction(data)])

		// close modal
		toggleModal()
		return this.setState({confirmModal: true})
	}

	render() {
		const {toggleModal} = this.props

		const actions = [
			<FlatButton
				label="Cancelar"
				primary={true}
				onClick={toggleModal}
			/>,
			<FlatButton
				label="Apagar Dados"
				secondary={true}
				onClick={this.deleteData}
			/>,
		]

		return (
			<div>
				<Dialog title="Notificação" open={this.state.confirmModal} actions={<FlatButton primary={true} onClick={() => this.setState({confirmModal: false})} label="Ok"/>}>Dados excluido com sucesso!</Dialog>

				<Dialog
					title="Notificação"
					actions={actions}
					modal={false}
					open={this.props.openModal}
					onRequestClose={() => this.props.toggleModal()}
				>
                    Você deseja deletar a tabela e todos os dados contidos?
				</Dialog>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		_id: state._id,
		...state.initialState
	}
}

export default connect(mapStateToProps)(DialogDeleteComp)
// CODE CLEANED!