import React, { Component } from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as TableActionCreator from '../../actions/table'
import {updateTable} from '../../API/ProductAPI'

import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'

class DialogSaveComp extends Component {
	state = {
		confirmModal: false
	}

	saveData = () => {
		const {products, uniqueID, toggleModal, _id, dispatch} = this.props
		const updateTableAction = bindActionCreators(TableActionCreator.populateTable, dispatch)

		updateTable({_id, products, uniqueID})
		.then(({data}) => updateTableAction(data))

		// after update table, close modal
		toggleModal()
		return this.setState({confirmModal: true})
	}

	render() {
		const {toggleModal} = this.props

		const actions = [
			<FlatButton
				label="Cancelar"
				primary={true}
				onClick={toggleModal}
			/>,
			<FlatButton
				label="Salvar Dados"
				secondary={true}
				onClick={this.saveData}
			/>,
		]

		return (
			<div>
				<Dialog title="Notificação" open={this.state.confirmModal} actions={<FlatButton primary={true} onClick={() => this.setState({confirmModal: false})} label="Ok"/>}>{'Tabela salva com sucesso!'}</Dialog>

				<Dialog
					title="Notificação"
					actions={actions}
					modal={false}
					open={this.props.openModal}
					onRequestClose={() => this.props.toggleModal()}
				>
                    Você deseja salvar a tabela com seus dados contidos?
				</Dialog>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		tableList: state.tableList,
		_id: state._id,
		...state.initialState
	}
}

export default connect(mapStateToProps)(DialogSaveComp)
// CODE CLEANED!