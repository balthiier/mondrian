import React, { Component } from 'react';
import {connect} from 'react-redux'

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import PrintIcon from 'material-ui/svg-icons/action/print';
import AssignIcon from 'material-ui/svg-icons/action/assignment-turned-in';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import InsertChartIcon from 'material-ui/svg-icons/editor/insert-chart';

import MenuDrawer from './MenuDrawer'
import DialogSaveComp from './DialogSaveComp'
import DialogDeleteComp from './DialogDeleteComp'
import Statistics from './Statistics'

class Header extends Component { 
	state = {
		openDrawer: false,
		openSaveModal: false,
		openDeleteModal: false,
		openStatisticsModal: false
	}

	toggleDrawer = () => {
		return this.setState({openDrawer: !this.state.openDrawer})
	}

	toggleSaveModal = () => {
		return this.setState({openSaveModal: !this.state.openSaveModal})
	}
	
	toggleDeleteModal = () => {
		return this.setState({openDeleteModal: !this.state.openDeleteModal})
	}

	toggleStatistics = () => {
		return this.setState({openStatisticsModal: !this.state.openStatisticsModal})
	}
	
	printTable = () => setTimeout(() => window.print(), 200)

    render() {
		const {tableList} = this.props
		
		const Options = () => (
			tableList.length > 0 &&
			<IconMenu
				iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
				targetOrigin={{horizontal: 'right', vertical: 'top'}}
				anchorOrigin={{horizontal: 'right', vertical: 'top'}}
			>
				<MenuItem leftIcon={<AssignIcon/>} primaryText="Salvar Tabela" onClick={this.toggleSaveModal}/>		
				<MenuItem leftIcon={<DeleteIcon/>} primaryText="Limpar Tabela" onClick={this.toggleDeleteModal}/>
				<MenuItem leftIcon={<PrintIcon/>} primaryText="Imprimir Tabela" onClick={this.printTable}/>
				<MenuItem leftIcon={<InsertChartIcon/>} primaryText="Estatística" onClick={this.toggleStatistics}/>
			</IconMenu>
		);

        return (
            <div>
				{/* DRAWER APP */}
				<MenuDrawer openDrawer={this.state.openDrawer} toggleDrawer={this.toggleDrawer}/>

				{/* APP MODAL GOES HERE */}
                <DialogSaveComp toggleModal={this.toggleSaveModal} openModal={this.state.openSaveModal}/>
                <DialogDeleteComp toggleModal={this.toggleDeleteModal} openModal={this.state.openDeleteModal}/>
				<Statistics toggleModal={this.toggleStatistics} openModal={this.state.openStatisticsModal}/>

				{/* MENU BAR */}
                <AppBar title="MENU" showMenuIconButton={true} onLeftIconButtonTouchTap={() => this.setState({openDrawer: !this.state.openDrawer})} iconElementRight={<Options/>}/>
            </div>
        );
    }
}

const mapStateToProps = state => {
	return {
		tableList: [...state.tableList]
	}
}

export default connect(mapStateToProps)(Header)
// MUST CLEAN