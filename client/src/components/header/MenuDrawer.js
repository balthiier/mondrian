import React, { Component } from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as TableActionCreator from '../../actions/table'
import * as SnackbarActionCreator from '../../actions/snackbar'
import {createTable, deleteTable, showTables} from '../../API/ProductAPI'

import logo from '../../mondrian-logo.png'
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import Drawer from 'material-ui/Drawer';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import NoteAddIcon from 'material-ui/svg-icons/action/note-add';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import FlatButton from 'material-ui/FlatButton'

class MenuDrawer extends Component {
	actionDrawer (type, action) {
		const {dispatch} = this.props
		const populateTable = bindActionCreators(TableActionCreator.populateTable, dispatch)
        const selectTable = bindActionCreators(TableActionCreator.selectTable, dispatch)
        const toggleSnackbar = bindActionCreators(SnackbarActionCreator.toggleSnackbar, dispatch)

		switch(type) {
			case 'addNewTable': {
                // create table from DB, then return the last table existing of the list
                return createTable().then(({data}) => [populateTable(data), selectTable(data[data.length - 1])])
			}

			case 'deleteTable': {
				// delete table from DB, then return the last table existing of the list
				return deleteTable(action)
                .then(({data}) => [populateTable(data), selectTable(data[data.length - 1])])
                // show snackbar, then hide after 4 seconds (FIND A BETTER SOLUTION)
                .then(() => toggleSnackbar())
                .then(() => setTimeout(() => toggleSnackbar(), 4500))
			}

			case 'selectTable': {
                // select table
				return selectTable(action)
			}

			case 'loadData': {
                // load tables
				return showTables()
				.then(({data}) => [populateTable(data), selectTable(data[0])])
				.catch(err => setTimeout(() => this.actionDrawer('loadData'), 2500))
			}

			default: {
				return null
			}
		}
	}

	componentWillMount() {
		return this.actionDrawer('loadData')
	}

    render() {
        const {tableList} = this.props
        
        return (
            <Drawer
                docked={false}
                width={300}
                open={this.props.openDrawer}
                onRequestChange={(open) => this.props.toggleDrawer(open)}
            >
                <List style={{paddingTop: 0}}>
                    <div className="logo">
                        <img src={logo} alt="logo"/>
                    </div>
                    <FlatButton className="addButton" primary={true} style={{height: 80}} fullWidth={true} label="Nova tabela" icon={<NoteAddIcon/>} onClick={() => this.actionDrawer('addNewTable')}/>						
                    <Subheader inset={false}>Tabelas</Subheader>
                    <div className="tableList">
                        {
                            // repeat number of table in DB
                            tableList.map((table, i) => {
                                return <ListItem
                                    key={i}
                                    leftAvatar={<Avatar icon={<ActionAssignment />} backgroundColor={"#2196F3"} />}
                                    primaryText={table.name}
                                    secondaryText={'Atualizado em: ' + new Date(table.updatedAt).toLocaleDateString()}
                                    rightIconButton={
                                        <IconMenu iconButtonElement={
                                            <IconButton touch={true}>
                                                <DeleteIcon color={'#757575'}/>
                                            </IconButton>
                                        }>
                                            <MenuItem style={{color: '#df0010'}} onClick={() => this.actionDrawer('deleteTable', table._id)}>Apagar esta tabela</MenuItem>
                                            <MenuItem>Cancelar</MenuItem>
                                        </IconMenu>
                                    }
                                    onClick={() => [this.actionDrawer('selectTable', table), this.props.toggleDrawer()]}
                                />
                            })
                        }
                    </div>
                </List>
            </Drawer>
        );
    }
}

const mapStateToProps = state => {
	return {
		tableList: [...state.tableList]
	}
}

export default connect(mapStateToProps)(MenuDrawer)
// CODE CLEANED