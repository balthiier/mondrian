import React from 'react'
import {connect} from 'react-redux'

import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'

import {Doughnut} from 'react-chartjs-2'

const Statistics = props => {
    const {products} = props
    const discount = products.reduce((actual, product) => (actual * 1 + product.withTax * product.quantity * 100 / 100), 0)
    const noDiscount = products.reduce((actual, product) => (actual * 1 + product.noTax * product.quantity * 100 / 100), 0)
	const material = noDiscount * 0.60;
	const handWork = noDiscount * 0.40;

    return (
        <Dialog
            className="statistics"
            title="Estatística da tabela"
            actions={<FlatButton primary={true} onClick={() => props.toggleModal()} label="OK"/>}
            open={props.openModal}
            onRequestClose={() => props.toggleModal()}
        >
            <Doughnut data={{
                datasets: [{
                    data: [discount.toFixed(2), noDiscount.toFixed(2), (noDiscount - discount).toFixed(2), handWork.toFixed(2), material.toFixed(2)],
                    backgroundColor: ['#0046a4', '#df0010', '#fff000', '#ff4081', '#5540ff']
                }],
                labels: ['BDI(R$)', 'C/BDI(R$)', 'S/BDI(R$)', 'Mão de obra(R$)', 'Material(R$)']
            }}/>
        </Dialog>
    )
}

const mapStateToProps = ({initialState}) => {
    return {
        products: initialState.products
    }
}

export default connect(mapStateToProps)(Statistics)
// CODE CLEANED!