import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import * as ProductActionCreators from '../actions/product'
import axios from 'axios'
import logo from '../mondrian-logo.png'
import URL from '../config/config'

import Header from '../components/header/Header'
import ProductForm from '../components/ProductForm'
import ProductTableDetails from '../components/ProductTableDetails'

import CircularProgress from 'material-ui/CircularProgress';
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import Snackbar from 'material-ui/Snackbar';

class App extends Component {
	state = {
		selected: [],
		title: "Tabela sem nome",
		titleToggle: false
	}

	// select rows of table component
	selectedProducts = selected => {
		return this.setState({selected})
	}

	getMappedProducts = selected => {
		const {dispatch} = this.props		
		const selectProduct = bindActionCreators(ProductActionCreators.selectProduct, dispatch)
		
		// map with selected indexes to be removed of array in redux's state
		const indexedProduct = selected.map(index => this.props.products[index])

		// sent to dispatch redux
		selectProduct(indexedProduct)
		return this.setState({selected: []})
	}


	updateTitle = e => {
		const {dispatch, _id} = this.props

		e.preventDefault()
		// send via AJAX to BACKEND the updated value, and return updated tables
		axios.patch(`${URL.host}updatetable/`, {data: {_id, name: this.state.title}})
		.then(({data}) => {
			dispatch({
				type: 'table/CHANGE_NAME_TABLE',
				name: this.state.title
			})
			dispatch({
				type: 'table/POPULATE_TABLE',
				tables: data
			})
			this.setState({titleToggle: false})
		})

		return;
	}

	render() {
		const {dispatch} = this.props
		const addProduct = bindActionCreators(ProductActionCreators.addProduct, dispatch)

		return (
			<div>
				<Header/>
				{/* logo for print purpose only */}
				<img src={logo} alt="logo" className='printLogo'/>
				
				{/* loading component will be gone when data be fetched on Header component */}
				{this.props.loading && <div style={{display: 'flex', marginTop: 200, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', textAlign: 'center'}}><CircularProgress size={60} thickness={7} /><h2 style={{fontWeight: 400}}>Carregando...</h2></div>}

				{this.props.tableList.length > 0 ? <div>
					{!this.state.titleToggle && 
						<h1 style={{textAlign: 'center', cursor: 'pointer', marginBottom: 14, height: 43}} onClick={()=> this.setState({titleToggle: !this.state.titleToggle})}>{this.props.name || 'Tabela sem nome'}</h1>
					}

					{this.state.titleToggle &&
						<form style={{textAlign: 'center', marginBottom: 6}} onSubmit={this.updateTitle}>
							<TextField
								name="titleName"
								type="text"
								minLength={3}
								floatingLabelText="Título da tabela"
								autoFocus
								required
								onChange={e=> this.setState({title: e.target.value})}
								onBlur={()=> this.setState({titleToggle: false})}
							/>
						</form>
					}

					<ProductForm addProduct={addProduct}/>
					<ProductTableDetails selectedProducts={this.selectedProducts}/>

					{this.state.selected.length > 0 && <RaisedButton icon={<DeleteIcon />} secondary={true} style={style.button} onClick={()=> this.getMappedProducts(this.state.selected)} label="APAGAR"/>}

					{/* SNACKBAR */}
					<Snackbar
						open={this.props.openSnack}
						message="Tabela deletada com sucesso!"
						style={{textAlign: 'center'}}
					/>
				</div>: !this.props.loading &&

				<div style={{textAlign: 'center', color: '#888', marginTop: 100}}>
					<h2 style={{fontWeight: 500}}>Não há nenhuma tabela criada.</h2>
					<h3 style={{fontWeight: 400}}>Para criar, clique no ícone no canto superior do menu.</h3>
				</div>}
			</div>
		);
	}
}

const style = {
	button: {position: 'absolute', bottom: 18, left: 10, zIndex: 1}
}

const mapStateToProps = state => {
	return {
		openSnack: state.openSnack,
		loading: state.loading,
		name: state.name,
		_id: state._id,
		tableList: state.tableList,
		products: state.initialState.products
	}
}

export default connect(mapStateToProps)(App)
// MUST CLEAN CODE