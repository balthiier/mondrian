import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import { createStore } from 'redux'
import {Provider} from 'react-redux'
import Product from './reducer/product'

const store = createStore(Product, window.devToolsExtension && window.devToolsExtension())

const muiTheme = getMuiTheme({
  textField: {
    floatingLabelColor: '#0046a4',
    focusColor: '#0046a4',
    textColor: '#37474F'
  },
  appBar: {
    textColor: 'white',
    color: '#0046a4'

  },
  flatButton: {
    primaryTextColor: '#0046a4',
    secondaryTextColor: '#df0010'
  },
  raisedButton: {
    primaryColor: '#0046a4',
    secondaryColor: '#df0010'
  },
  svgIcon: {
    color: 'white'
  },
  checkbox: {
    checkedColor: '#0046a4',
    boxColor: '#0046a4'
  }
});

ReactDOM.render(
<MuiThemeProvider muiTheme={muiTheme}>
  <Provider store={store}>
    <App/>
  </Provider>
</MuiThemeProvider>
, document.getElementById('root'));

registerServiceWorker();