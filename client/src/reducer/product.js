import * as ProductActionType from '../actionTypes/product'
import * as TableActionType from '../actionTypes/table'
import * as SnackbarActionType from '../actionTypes/snackbar'

const localInitialState =  {
    name: "tabela sem nome",
    initialState: {
        products: [],
        uniqueID: 0
	},
	tableList: [],
	loading: true,
	openSnack: false
}

export default function Product(state = localInitialState, action) {
	switch(action.type) {
	case ProductActionType.ADD_PRODUCT: {
		const newProduct = {...action.product, uniqueID: state.initialState.uniqueID}
		return {
			...state,
			initialState: {
				products: [...state.initialState.products, newProduct],
				uniqueID: state.initialState.uniqueID + 1
			}
		}
	}

	case ProductActionType.SELECT_PRODUCT: {
		// TODO: remove index of products state by uniqueID 
		const products = state.initialState.products.filter(product => {
			for(let i=0; i <= action.index.length; i++) {
				if(typeof action.index[i] !== 'undefined') {
					if(product.uniqueID === action.index[i].uniqueID) return false						
				}
			}
			return true
		})

		return {
			...state,
			initialState: {
				...state.initialState,
				products
			}
		} 
	}

	case ProductActionType.REMOVEALL_PRODUCT: {
		return {
			...state,
			initialState: {
				products: [],
				uniqueID: 0
			}
		}
	}

	case TableActionType.POPULATE_TABLE: {
		return {
			...state,
			tableList: action.tables,
			loading: false
		}
	}

	case TableActionType.SELECT_TABLE: {
		const newState = action.table
		return {
			...state,
			...newState
		}
	}

	case TableActionType.CHANGE_NAME_TABLE: {
		const name = action.name
		return {
			...state,
			name
		}
	}

	case SnackbarActionType.TOGGLE_SNACKBAR: {
		return {
			...state,
			openSnack: !state.openSnack
		}
	}

	default: {
			return state
		}
	}
}