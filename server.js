require('./src/config/metrics') //CALL METRICS
const path = require('path')
const express= require('express')
const app = express()
const port = require('./src/config/config').port

//require Mongoose
require('./src/config/mongoose')
require('./src/config/express')(express, app)

// hey listen!
app.listen(port, () => console.log(`running at ${port}`))