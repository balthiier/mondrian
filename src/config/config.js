const port = process.env.PORT || 3000

const db = {
    test: 'mongodb://localhost:27017/calculation',
    prod: 'mongodb://mondrian:assis2017@ds157325.mlab.com:57325/calculation'
}

module.exports = {
    port,
    db
}