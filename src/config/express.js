const bodyParser = require('body-parser')
// const morgan = require('morgan')
const path = require('path')

module.exports = (express, app) => {
    // config use of static assets
    app.use(express.static(path.resolve(__dirname, '../../client/build')))
    // config use of morgan logger
    // app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))
    // config use of bodyparser
    app.use(bodyParser.urlencoded({extended: true}))
    app.use(bodyParser.json())
    // enable CORS
    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*")
        res.header("Access-Control-Allow-Methods", "GET, PUT, POST, PATCH, DELETE, HEAD, OPTIONS")
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
        next()
    })      
    // call routes
    require('../routes/tables')(app)

    // if page do not exists, redirect to homepage
    app.get('*', (req, res) => res.sendFile(path.resolve(__dirname, '../client/build/index.html')))
}