const probe = require('pmx').probe()
const mongoose = require('./mongoose')
const tables = require('../models/table')

const tableMetric = (tableLength = 0) => probe.metric({
  name    : 'Licitações Ativas',
  value   : function() {
    return tableLength
  }
});

// find in DB all number of tables
setTimeout(() => tables.find({}, (err, tables) => tableMetric(tables.length )), 60000)