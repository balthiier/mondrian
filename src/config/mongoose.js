const mongoose = require('mongoose')
mongoose.Promise = Promise
const db = require('./config').db

mongoose.connect(db.prod, {useMongoClient: true, keepAlive: 120}, err => {
    if(err) console.error(err)

    console.log('DB connected!')
})

module.exports = mongoose