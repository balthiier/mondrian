const db = require('../config/mongoose')
const tableModel = require('../models/table')

// show all tables of DB
const showTables = (req, res) => {
     tableModel.find({}, (err, tables) => {
        if(err) return res.status(404).send({'status': res.statusCode, 'message': 'Falha ao connectar com o DB, tente mais tarde.'})

        return res.status(200).send(tables)
    })
}

// create and then show all existing tables
const createTable = (req, res) => {
    tableModel.create({}, err => {
        if(err) return res.status(500).send({'status': res.statusCode, 'message': 'Falha ao criar a tabela, tente mais tarde.'})        

        return showTables(req, res)
    })
}

// update then return all tables
const updateTable = (req, res) => {
    const body = req.body.data

    tableModel.findByIdAndUpdate({_id: body._id}, {...body, updatedAt: new Date(), new: true}, err => {
        if(err) return res.status(500).send({'status': res.statusCode, 'message': 'Falha ao atualizar dados da tabela, tente mais tarde.'})    

        return showTables(req, res)
    })
}

// delete then return all tables
const deleteTable = (req, res) => {
    tableModel.findByIdAndRemove(req.body._id, err => {
        if(err) return res.status(500).send({'status': res.statusCode, 'message': 'Falha ao atualizar dados da tabela, tente mais tarde.'})    
        
        return showTables(req, res)
    })
}

module.exports = {
    showTables,
    createTable,
    updateTable,
    deleteTable
}