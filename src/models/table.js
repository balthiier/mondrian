const db = require('../config/mongoose')
const Schema = db.Schema

const TableSchema = new Schema({
    name: {
        type: String,
        minlength: 3,
        required: true,
        default: 'tabela sem nome'
    },
    updatedAt: {
        type: Date,
        default: Date.now,
        required: true
    },
    initialState: {
        products: {
            type: Array,
            default: []
        },
        uniqueID: {
            type: Number,
            default: 0
        }
    }
})

const tableModel = db.model('Table', TableSchema)

module.exports = tableModel