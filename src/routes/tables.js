const tableControllers = require('../controllers/table')

module.exports = app => {
    app.post('/createtable', tableControllers.createTable)    
    app.get('/showtables', tableControllers.showTables)
    app.patch('/updatetable', tableControllers.updateTable)
    app.delete('/deletetable', tableControllers.deleteTable)
}